package com.springcloud.commons.rest.marvel;

import com.springcloud.commons.rest.marvel.dto.SecurityMarvelParamsDto;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@Slf4j
public class SecurityMarvelConfig {

    public static final String UTF_8 = "UTF-8";
    public static final String MD_5 = "MD5";

    @Value("${marvel.api.private.key}")
    private String privateKey;

    @Value("${marvel.api.public.key}")
    private String publicKey;

    @Value("${marvel.host}")
    @Getter
    private String marvelHost;

    SecurityMarvelParamsDto getSecurityParams() {
        SecurityMarvelParamsDto securityParamsDto = new SecurityMarvelParamsDto();
        securityParamsDto.setTs(String.valueOf(new Date().getTime()));
        securityParamsDto.setApikey(publicKey);
        securityParamsDto.setHash(getHashParam(securityParamsDto.getTs()));
        securityParamsDto.setHost(marvelHost);
        return securityParamsDto;
    }

    private String getHashParam(String timeStamp) {
        try {
            byte[] bytesOfMessage = timeStamp.concat(privateKey).concat(publicKey).getBytes(UTF_8);
            MessageDigest md = MessageDigest.getInstance(MD_5);
            md.update(bytesOfMessage);
            return String.valueOf(Hex.encodeHex(md.digest()));
        } catch (UnsupportedEncodingException e) {
            log.error("UnsupportedEncodingException",e);
        } catch (NoSuchAlgorithmException e) {
            log.error("NoSuchAlgorithmException",e);
        }
        return null;
    }
}
