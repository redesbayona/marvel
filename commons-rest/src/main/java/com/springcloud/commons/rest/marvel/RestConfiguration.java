package com.springcloud.commons.rest.marvel;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan("com.springcloud.commons.rest.marvel")
@PropertySource("classpath:marvel.properties")
public class RestConfiguration {

    @Bean
    public SecurityMarvelConfig securityMarvelConfig() {
        return new SecurityMarvelConfig() {
            @Override
            public String getMarvelHost() {
                return super.getMarvelHost();
            }
        };
    }

    @Bean
    public RestTemplate getRestTemplate(){
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate;
    }
}
