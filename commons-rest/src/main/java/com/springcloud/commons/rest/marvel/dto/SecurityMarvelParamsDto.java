package com.springcloud.commons.rest.marvel.dto;

import lombok.Data;

@Data
public class SecurityMarvelParamsDto {

    private String ts;
    private String hash;
    private String apikey;
    private String host;

}
