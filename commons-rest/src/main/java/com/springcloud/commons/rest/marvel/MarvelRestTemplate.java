package com.springcloud.commons.rest.marvel;

import com.springcloud.commons.rest.marvel.dto.SecurityMarvelParamsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

@Component
public class MarvelRestTemplate {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SecurityMarvelConfig securityMarvelConfig;

    @Value("${marvel.param.ts}")
    private String tsParam;

    @Value("${marvel.param.apikey}")
    private String apiKeyParam;

    @Value("${marvel.param.hash}")
    private String hashParam;

    public Object consultMarvelApi(final String entityName, Class responseClass, Map<String, String> params) {
        SecurityMarvelParamsDto securityParams = securityMarvelConfig.getSecurityParams();
        UriComponentsBuilder uriComponentsBuilder = getUriComponentsBuilder(entityName, securityParams);
        addParameters(params, uriComponentsBuilder);
        return restTemplate.getForObject(uriComponentsBuilder.build().encode().toUri(), responseClass);
    }

    private void addParameters(Map<String, String> params, UriComponentsBuilder uriComponentsBuilder) {
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue());
            }
        }
    }

    private UriComponentsBuilder getUriComponentsBuilder(String entityName, SecurityMarvelParamsDto securityParams) {
        return UriComponentsBuilder.fromHttpUrl(securityParams.getHost().concat(entityName))
                .queryParam(tsParam, securityParams.getTs())
                .queryParam(apiKeyParam, securityParams.getApikey())
                .queryParam(hashParam, securityParams.getHash());
    }
}
