package com.springcloud.characters.rest;

import com.springcloud.characters.rest.dto.CharacterDto;
import com.springcloud.characters.rest.dto.CharactersDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;

@RestController
public interface ICharactersRest {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    CharactersDto listCharacters(@QueryParam("offset") String offset,
                                 @QueryParam("limit") String limit);

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    CharacterDto getCharacter(@PathVariable("id") String id);
}
