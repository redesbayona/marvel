package com.springcloud.characters.rest.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CharacterDto implements Serializable{

    private String id;
    private String name;
    private String description;
    private Date modified;
    private String thumbnail;
    private String resourceURI;
}
