package com.springcloud.characters.rest.dto;

import com.springcloud.commons.api.rest.dto.PaginationDto;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CharactersDto extends PaginationDto{

    private List<CharacterDto> characters;
}
