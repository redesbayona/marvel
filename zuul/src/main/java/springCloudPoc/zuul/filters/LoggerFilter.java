package springCloudPoc.zuul.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.log4j.Logger;
import springCloudPoc.zuul.filters.enums.ZuulFilterTypeEnum;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by bayona on 31/08/2016.
 */
public class LoggerFilter extends ZuulFilter {

    public static final transient Logger log = Logger.getLogger(LoggerFilter.class);


    @Override
    public String filterType() {
        return ZuulFilterTypeEnum.PRE.getCode();
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.error("Zuul filter active for request : " + request.getRequestURL());
        return null;
    }
}
