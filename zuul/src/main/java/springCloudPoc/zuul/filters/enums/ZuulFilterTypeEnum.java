package springCloudPoc.zuul.filters.enums;

/**
 * Created by bayona on 31/08/2016.
 */

public enum ZuulFilterTypeEnum {
    PRE("pre"), ROUTING("routing"), POST("post"), ERROR("error");

    private String code;

    ZuulFilterTypeEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
