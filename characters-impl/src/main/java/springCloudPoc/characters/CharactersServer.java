package springCloudPoc.characters;


import com.springcloud.commons.rest.marvel.RestConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class })
@Configuration
@ComponentScan(basePackages = {"com.springcloud.characters.repositories", "com.springcloud.characters.rest.impl",
        "com.springcloud.characters.services","com.springcloud.mavel.api.repositories.mappers"})
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@PropertySource("classpath:characters.properties")
@Import(RestConfiguration.class)
public class CharactersServer {
    public static void main(String[] args) {
        SpringApplication.run(CharactersServer.class, args);
    }

}
