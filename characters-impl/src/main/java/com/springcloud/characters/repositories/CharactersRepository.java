package com.springcloud.characters.repositories;

import com.springcloud.characters.repositories.models.CharacterModel;
import com.springcloud.characters.repositories.models.CharactersModel;
import com.springcloud.characters.repositories.models.CharactersResponseModel;
import com.springcloud.commons.rest.marvel.MarvelRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class CharactersRepository implements ICharactersRepository {

    @Value("${marvel.rest.entity.results}")
    private String charactersEntity;

    @Autowired
    private MarvelRestTemplate marvelRestTemplate;

    @Override
    public CharacterModel getCharacter(String id) {
        CharactersResponseModel charactersResponseModel = (CharactersResponseModel) marvelRestTemplate.consultMarvelApi(charactersEntity.concat("/").concat(id), CharactersResponseModel.class, null);
        return charactersResponseModel.getData().getResults().get(0);
    }

    @Override
    public CharactersModel listCharacters(String offset, String limit) {
        Map<String, String> params = addParameters(offset, limit);
        CharactersResponseModel charactersResponseModel = (CharactersResponseModel) marvelRestTemplate.consultMarvelApi(charactersEntity, CharactersResponseModel.class, params);
        return charactersResponseModel.getData();
    }

    private Map<String, String> addParameters(String offset, String limit) {
        if (offset != null && limit != null) {
            LinkedHashMap<String, String> params = new LinkedHashMap<>();
            params.put("offset", offset);
            params.put("limit", limit);
            return params;
        }
        return null;
    }
}
