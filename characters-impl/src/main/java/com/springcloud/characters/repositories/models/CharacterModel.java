package com.springcloud.characters.repositories.models;

import com.springcloud.mavel.api.repositories.models.TumbnailModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CharacterModel implements Serializable {

    private String id;
    private String name;
    private String description;
    private Date modified;
    private TumbnailModel thumbnail;
    private String resourceURI;
}
