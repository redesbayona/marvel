package com.springcloud.characters.repositories.models;

import com.springcloud.mavel.api.repositories.models.PaginationModel;
import lombok.Data;

import java.util.List;

@Data
public class CharactersModel extends PaginationModel {

    private List<CharacterModel> results;
}
