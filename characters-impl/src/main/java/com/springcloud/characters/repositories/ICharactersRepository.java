package com.springcloud.characters.repositories;

import com.springcloud.characters.repositories.models.CharacterModel;
import com.springcloud.characters.repositories.models.CharactersModel;

public interface ICharactersRepository {

    CharacterModel getCharacter(String id);

    CharactersModel listCharacters(String offset, String limit);
}
