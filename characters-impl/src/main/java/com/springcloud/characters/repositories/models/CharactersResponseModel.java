package com.springcloud.characters.repositories.models;

import com.springcloud.mavel.api.repositories.models.PaginationModel;
import lombok.Data;

@Data
public class CharactersResponseModel extends PaginationModel{

    private CharactersModel data;
}
