package com.springcloud.characters.services.mappers;

import com.springcloud.characters.repositories.models.CharacterModel;
import com.springcloud.characters.rest.dto.CharacterDto;
import org.springframework.stereotype.Component;
import springCloudPoc.webUtils.commons.mapper.IMapper;

@Component
public class CharacterMapper implements IMapper<CharacterDto, CharacterModel>{

    @Override
    public CharacterDto mapToOuter (CharacterModel model){
        CharacterDto dto = new CharacterDto();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setDescription(model.getDescription());
        dto.setModified(model.getModified());
        dto.setThumbnail(model.getThumbnail().getPath().concat(".").concat(model.getThumbnail().getExtension()));
        dto.setResourceURI(model.getResourceURI());
        return dto;
    }

}
