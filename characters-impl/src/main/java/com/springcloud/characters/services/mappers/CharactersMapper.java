package com.springcloud.characters.services.mappers;

import com.springcloud.characters.repositories.models.CharactersModel;
import com.springcloud.characters.rest.dto.CharactersDto;
import com.springcloud.mavel.api.repositories.mappers.PaginationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import springCloudPoc.webUtils.commons.mapper.IMapper;

@Component
public class CharactersMapper implements IMapper<CharactersDto, CharactersModel> {

    @Autowired
    private CharacterMapper characterMapper;

    @Autowired
    private PaginationMapper paginationMapper;

    @Override
    public CharactersDto mapToOuter(CharactersModel model) {
        CharactersDto dto = new CharactersDto();
        paginationMapper.map(model, dto);
        dto.setCharacters(characterMapper.mapToOuterList(model.getResults()));
        return dto;
    }

}
