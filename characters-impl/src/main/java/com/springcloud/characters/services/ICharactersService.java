package com.springcloud.characters.services;

import com.springcloud.characters.rest.dto.CharacterDto;
import com.springcloud.characters.rest.dto.CharactersDto;

public interface ICharactersService {

    CharacterDto getCharacter(String id);

    CharactersDto listCharacters(String offset, String limit);
}
