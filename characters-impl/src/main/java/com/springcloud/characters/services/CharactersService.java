package com.springcloud.characters.services;

import com.springcloud.characters.repositories.ICharactersRepository;
import com.springcloud.characters.repositories.models.CharacterModel;
import com.springcloud.characters.repositories.models.CharactersModel;
import com.springcloud.characters.rest.dto.CharacterDto;
import com.springcloud.characters.rest.dto.CharactersDto;
import com.springcloud.characters.services.mappers.CharacterMapper;
import com.springcloud.characters.services.mappers.CharactersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CharactersService implements ICharactersService {

    @Autowired
    private ICharactersRepository charactersRepository;

    @Autowired
    private CharacterMapper characterMapper;

    @Autowired
    private CharactersMapper charactersMapper;

    @Override
    public CharacterDto getCharacter(String id) {
        CharacterModel character = charactersRepository.getCharacter(id);
        return characterMapper.mapToOuter(character);
    }

    @Override
    public CharactersDto listCharacters(String offset, String limit) {
        CharactersModel charactersModel = charactersRepository.listCharacters(offset, limit);
        return charactersMapper.mapToOuter(charactersModel);
    }
}
