package com.springcloud.characters.rest.impl;

import com.springcloud.characters.rest.ICharactersRest;
import com.springcloud.characters.rest.dto.CharacterDto;
import com.springcloud.characters.rest.dto.CharactersDto;
import com.springcloud.characters.services.ICharactersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;

@RestController
public class CharactersRest implements ICharactersRest {

    @Autowired
    private ICharactersService charactersService;

    @Override
    public CharacterDto getCharacter(@PathVariable("id") String id) {
        return charactersService.getCharacter(id);
    }

    @Override
    public CharactersDto listCharacters(@QueryParam("offset") String offset,
                                        @QueryParam("limit") String limit) {
        return charactersService.listCharacters(offset, limit);
    }
}
