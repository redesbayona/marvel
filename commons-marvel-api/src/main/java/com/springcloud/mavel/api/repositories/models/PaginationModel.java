package com.springcloud.mavel.api.repositories.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class PaginationModel implements Serializable{

    private Integer offset;
    private Integer limit;
    private Integer total;
    private Integer count;
}
