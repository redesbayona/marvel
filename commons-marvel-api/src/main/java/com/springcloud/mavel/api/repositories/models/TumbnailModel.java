package com.springcloud.mavel.api.repositories.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class TumbnailModel implements Serializable{

    private String path;
    private String extension;
}
