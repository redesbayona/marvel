package com.springcloud.mavel.api.repositories.mappers;

import com.springcloud.commons.api.rest.dto.PaginationDto;
import com.springcloud.mavel.api.repositories.models.PaginationModel;
import org.springframework.stereotype.Component;
import springCloudPoc.webUtils.commons.mapper.IMapper;

@Component
public class PaginationMapper implements IMapper<PaginationDto, PaginationModel> {

    public PaginationDto mapToOuter(final PaginationModel inner) {
        PaginationDto dto = new PaginationDto();
        map(inner, dto);
        return dto;

    }

    public void map(PaginationModel inner, final PaginationDto dto) {
        dto.setOffset(inner.getOffset());
        dto.setLimit(inner.getLimit());
        dto.setTotal(inner.getTotal());
        dto.setCount(inner.getCount());
    }

}
