package springCloudPoc.webUtils.commons.mapper;

import org.apache.commons.lang.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bayona on 29/08/2016.
 */
public interface IMapper<T, S> {

    default T mapToOuter(final S inner) {
        throw new NotImplementedException();
    }

    default S mapToInner(final T outer) {
        throw new NotImplementedException();
    }

    default List<T> mapToOuterList(final List<S> innerList) {
        if (innerList != null) {
            List<T> list = new ArrayList<>();
            for (S element : innerList) {
                list.add(mapToOuter(element));
            }
            return list;
        } else {
            return null;

        }
    }

    default List<S> mapToInnerList(final List<T> outerList) {
        if (outerList != null) {
            List<S> list = new ArrayList<>();
            for (T element : outerList) {
                list.add(mapToInner(element));
            }
            return list;
        } else {
            return null;

        }
    }

}
