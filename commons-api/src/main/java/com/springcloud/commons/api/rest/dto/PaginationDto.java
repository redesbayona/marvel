package com.springcloud.commons.api.rest.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PaginationDto implements Serializable{

    private Integer offset;
    private Integer limit;
    private Integer total;
    private Integer count;
}
